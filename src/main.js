import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router';
import Routes from '@/router.js';
import {store} from './store/store'
import './../node_modules/bootstrap/dist/css/bootstrap.min.css';

Vue.config.productionTip = false
Vue.use(VueRouter);

// Filter Starts //

Vue.filter('formatDate',function(value){
  if(value === 'NA'){
    return `NA`;
  } else if (value === 'Immediately') {
    return `Immediately`
  }else{
    var formattedDate = value.slice(0, 10).split('-');
    return `${formattedDate[2]}/${formattedDate[1]}/${formattedDate[0]}`;
  }
});

Vue.filter('breakLine',function(value){
  return value.replace(/\n/g,"<br/>");
})


// Filter Ends //


const router = new VueRouter({
  routes:Routes,
  mode:'history'
});

new Vue({
  render: h => h(App),
  router:router,
  store:store
}).$mount('#app')
