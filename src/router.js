import JobListings from '@/views/job-listing';
import SearchJob from '@/views/search-job';
import ListComponent from '@/components/list-component';

export default[
    {
        path:'/',component:JobListings
    },
    {
        path:'/search-job',component:SearchJob,
        children:[
            {
                    path: '*',
                    component: ListComponent,
                    name:'searchRoute',
                    props: (route) => ({
                        isSearch: route.query.isSearch,
                        location: route.query.location,
                        experience:route.query.experience
                    })
            }
        ]
    }
]