/* eslint-disable no-console */
/* eslint-disable no-unused-vars */

import Vue from 'vue';
import Vuex from 'vuex';
import VueResource from 'vue-resource'

Vue.use(VueResource);
Vue.use(Vuex);


export const store = new Vuex.Store({
    state: {
        jobList: []
    },
    getters: {
        JOBS: state =>{
            return state.jobList;
        }
    },
    actions: {
        GET_JOBLIST:(context,payload)=>{
            Vue.http.get('https://api.myjson.com/bins/kez8a').then(function(data){
                context.commit('INTIAL_SETUP',data.body.jobsfeed);
            })
        }
    },
    mutations: {
        INTIAL_SETUP: (state,payload)=>{
            state.jobList = payload;
        }
    }
});
